﻿using System;
using System.Threading.Tasks;
using Models;
using UnityEngine;
using Views;
using Views.UI;

namespace Controllers
{
    public class BlockController
    {
        private readonly IPanel _panel;
        private readonly IBlockView _blockView;
        private readonly IBlockModel _blockModel;

        private IMapModel _mapModel;

        /*public event EventHandler OnMoved;
        public event EventHandler OnRotated;*/
        
        public BlockController(IBlockView blockView, IPanel panel, IBlockModel blockModel)
        {
            _panel = panel;
            _blockView = blockView;
            _blockModel = blockModel;
            
            _blockModel.OnUnityPositionUpdated += OnUnityPositionUpdated;
            _blockModel.OnMoveStarted += OnMoveStarted;
            _blockModel.OnStepMade += OnStepMade;
        }

        private void OnStepMade(object sender, EventArgs e)
        {
            MadeStepEventArgs madeStepEventArgs = (MadeStepEventArgs) e;

            _blockView.MakeStep(madeStepEventArgs.NextUnityPosition, madeStepEventArgs.TimeInSeconds);
        }

        private void OnMoveStarted(object sender, EventArgs e)
        {
            _blockView.DrawFilledSells(_blockModel.Cells, _blockModel.Color);
            
            _panel.OnLeftButtonClick += OnLeftButtonClick;
            _panel.OnRightTurnButtonClick += OnRightTurnButtonClick;
            _panel.OnTurnButtonClick += OnTurnButtonClick;
        }

        public void Unsubscribe(IPanel panel)
        {
            panel.OnLeftButtonClick -= OnLeftButtonClick;
            panel.OnRightTurnButtonClick -= OnRightTurnButtonClick;
            panel.OnTurnButtonClick -= OnTurnButtonClick;
        }

        private void OnUnityPositionUpdated(object sender, EventArgs e)
        {
            UpdateUnityPositionEventArgs updateUnityPositionEventArgs = (UpdateUnityPositionEventArgs) e;
            _blockView.SetPosition(updateUnityPositionEventArgs.NewUnityPosition);
        }

        private void OnTurnButtonClick(object sender, EventArgs e)
        {
            _blockModel.Rotate();
        }

        private void OnRightTurnButtonClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        private void OnLeftButtonClick(object sender, EventArgs e)
        {
            throw new NotImplementedException();
        }
    }
}
