﻿using Models;
using UnityEngine;
using Views;

namespace Controllers
{
    public class MapController
    {
        private readonly IMapView _mapView;
        private readonly IMapModel _mapModel;

        private IBlockModel _blockModel;
        public MapController(IMapView mapView, IMapModel mapModel)
        {
            _mapView = mapView;
            _mapModel = mapModel;
        }

        public bool HasFreeCellsToStartMoving()
        {
            for (int i = 0; i < _mapModel.Size.x; i++)
            {
                if (!_mapModel.SellModels[i, _mapModel.Size.y -1].IsFilled)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
