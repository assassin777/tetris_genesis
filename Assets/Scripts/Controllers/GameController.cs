﻿using System;
using FSM;
using FSM.Factories;
using Models;
using Models.Factories.Block;
using Models.Factories.Map;
using UnityEngine;
using Views;
using Views.Factories.Block;

namespace Controllers
{
    public class GameController: IStateMachine
    {
        private readonly IGameView _gameView;
        private readonly IGameModel _gameModel;

        private IBlockViewFactory _blockViewFactory;
        private IBlockModelFactory _blockModelFactory;

        private MapController _mapController;
        private BlockController _currentBlockController;
        private IStateFactory _stateFactory;
        
        public IState State { get; private set; }
        public GameController(IGameView gameView, IGameModel gameModel)
        {
            _gameView = gameView;
            _gameModel = gameModel;
        }

        public void Start()
        {
            _gameModel.OnGameFinished += OnGameFinished;
            _gameModel.OnBlockCreated += OnBlockCreated;
            
            _blockViewFactory = new BlockViewFactory();
            _blockModelFactory = new BlockModelFactory();
            
            IMapModelFactory mapModelFactory = new MapModelFactory();
            IMapModel mapModel = mapModelFactory.MapModel(_gameModel.CellSize);

            _gameModel.AddMap(mapModel);
            
            _mapController = new MapController(_gameView.MapView, mapModel);
            _stateFactory = new StateFactory(this, _gameModel);
            
            SetState(StateType.StartGame);
        }

        private void OnBlockCreated(object sender, EventArgs e)
        {
            IBlockView blockView = _blockViewFactory.BlockView(_gameView.BlockParent);
            IBlockModel blockModel = _blockModelFactory.BlockModel(_gameModel.NextBlockType(), _gameModel.CellPerSecSpeed, _gameModel.CellSize);
            _currentBlockController = new BlockController(blockView, _gameView.Panel, blockModel);
            _gameModel.AddBlock(blockModel);
            _gameModel.FindStartPosition();
        }

        public void SetState(StateType stateType)
        {
            State = _stateFactory.GetState(stateType);
            State.EnterState();
        }
        
        private void OnGameFinished(object sender, EventArgs e)
        {
            _gameView.Panel.ShowFinishGameMassage();
        }
    }
}
