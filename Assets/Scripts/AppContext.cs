﻿using Controllers;
using Models;
using Models.Factories;
using UnityEngine;
using Views;

public class AppContext : MonoBehaviour
{
    [SerializeField] private GameView gameView;
    private void Start()
    {
        IGameModelFactory gameFactory = new GameModelFactory();
        IGameModel gameModel = gameFactory.GameModel();
        
        GameController gameController = new GameController(gameView, gameModel);

        gameController.Start();
    }
}
