﻿using System.Collections.Generic;
using UnityEngine;

namespace BalanceConfigs
{
    [CreateAssetMenu(fileName = "ComplexBlockConfig", menuName = "Configs/ComplexBlock")]
    public class ComplexBlockConfig : ScriptableObject
    {
        [SerializeField] private Dictionary<BlockTypes, BlockConfig> blockConfigs;

        public Dictionary<BlockTypes, BlockConfig> BlockConfigs => blockConfigs;
    }
}
