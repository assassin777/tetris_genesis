﻿using System.Collections.Generic;
using UnityEngine;

namespace BalanceConfigs
{
    [CreateAssetMenu(fileName = "LevelConfig", menuName = "Configs/Level", order = 1)]
    public class GameConfig : ScriptableObject
    {
        [SerializeField] private float cellPerSecSpeed;
        [SerializeField] private Dictionary<BlockTypes, int> blocksNumberByType;
        [SerializeField] private float cellSize;

        public float CellSize => cellSize;
        public Dictionary<BlockTypes, int> BlocksNumberByType => blocksNumberByType;

        public float CellPerSecSpeed => cellPerSecSpeed;
    }
}
