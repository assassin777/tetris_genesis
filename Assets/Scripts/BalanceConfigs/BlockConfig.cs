﻿using UnityEngine;

namespace BalanceConfigs
{
    [CreateAssetMenu(fileName = "BlockConfig", menuName = "Configs/lexBlock")]
    public class BlockConfig : ScriptableObject
    {
        [SerializeField] private Vector2Int[] filedCellPositions;
        
        public Vector2Int[] FiledCellPositions => filedCellPositions;
    }
}
