﻿using UnityEngine;

namespace Views.Factories.Block
{
    public interface IBlockViewFactory
    {
        IBlockView BlockView(Transform blockParent);
    }
}
