﻿using UnityEngine;

namespace Views.Factories.Block
{
    public class BlockViewFactory : IBlockViewFactory
    {
        public IBlockView BlockView(Transform blockParent)
        {
            GameObject prefab = Resources.Load<GameObject>("");
            GameObject instance = Object.Instantiate(prefab, blockParent);
            IBlockView blockView = instance.GetComponent<IBlockView>();
            return blockView;
        }
    }
}
