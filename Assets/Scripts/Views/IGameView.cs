﻿using UnityEngine;
using Views.UI;

namespace Views
{
    public interface IGameView
    {
        IMapView MapView { get; }
        Transform BlockParent { get; }
        IPanel Panel { get; }
    }
}
