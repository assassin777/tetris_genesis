﻿using System;
using System.Collections;
using UnityEngine;

namespace Views
{
    public class BlockView : MonoBehaviour, IBlockView
    {
        private SpriteRenderer[] _cellsRenderers;
        private Coroutine _makeStepCoroutine;

        private Vector2 _nextPosition;

        public event EventHandler OnStepFinished;
        
        private void Awake()
        {
            _cellsRenderers = GetComponentsInChildren<SpriteRenderer>();
            DisableRenderers();
        }
        
        public void SetPosition(Vector2 position)
        {
            transform.localPosition = position;
        }
        
        public void DrawFilledSells(bool[,] blockCells, Color color)
        {
            int length0 = blockCells.GetLength(0);
            for (int i = 0; i < length0; i++)
            {
                for (int j = 0; j < blockCells.GetLength(1); j++)
                {
                    SpriteRenderer spriteRenderer = _cellsRenderers[(i * length0) + j];
                    spriteRenderer.enabled = blockCells[i,j];
                    spriteRenderer.color = color;
                }
            }
        }

        public void MakeStep(Vector2 nextUnityPosition, float timeInSeconds)
        {
            _nextPosition = nextUnityPosition;
            _makeStepCoroutine = StartCoroutine(Wait(timeInSeconds, StepFinished));
        }

        private IEnumerator Wait(float timeInSeconds, Action OnWaitAction)
        {
            yield return new WaitForSeconds(timeInSeconds);
            OnWaitAction?.Invoke();
        }

        private void StepFinished()
        {
            transform.localPosition = _nextPosition;
            OnStepFinished?.Invoke(this, null);
        }

        private void DisableRenderers()
        {
            for (int i = 0; i < _cellsRenderers.Length; i++)
            {
                _cellsRenderers[i].enabled = false;
            }
        }
    }
}
