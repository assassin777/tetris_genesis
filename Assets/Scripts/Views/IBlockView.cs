﻿using UnityEngine;

namespace Views
{
    public interface IBlockView
    {
        void SetPosition(Vector2 position);
        void DrawFilledSells(bool[,] blockCells, Color color);
        void MakeStep(Vector2 nextUnityPosition, float timeInSeconds);
    }
}
