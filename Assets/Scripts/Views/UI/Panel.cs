﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Views.UI
{
    public class Panel : MonoBehaviour, IPanel
    {
        [SerializeField] private Button turn;
        [SerializeField] private Button left;
        [SerializeField] private Button right;

        [SerializeField] private Text message;

        public event EventHandler OnTurnButtonClick;
        public event EventHandler OnLeftButtonClick;
        public event EventHandler OnRightTurnButtonClick;

        
        public void ShowFinishGameMassage()
        {
            message.gameObject.SetActive(true);

            message.text = "Game over";
        }
        
        private void Awake()
        {
            message.gameObject.SetActive(false);
            
            turn.onClick.AddListener(TurnButtonClick);
            left.onClick.AddListener(LeftButtonClick);
            right.onClick.AddListener(RightButtonClick);
        }

        private void RightButtonClick()
        {
            OnRightTurnButtonClick?.Invoke(this, null);
        }

        private void LeftButtonClick()
        {
            OnLeftButtonClick?.Invoke(this, null);
        }

        private void TurnButtonClick()
        {
            OnTurnButtonClick?.Invoke(this, null);
        }
    }
}
