﻿using System;

namespace Views.UI
{
    public interface IPanel
    {
        event EventHandler OnTurnButtonClick;
        event EventHandler OnLeftButtonClick;
        event EventHandler OnRightTurnButtonClick;

        void ShowFinishGameMassage();
    }
}
