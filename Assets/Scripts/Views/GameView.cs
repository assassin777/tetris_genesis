﻿using UnityEngine;
using Views.UI;

namespace Views
{
    public class GameView : MonoBehaviour, IGameView
    {
        [SerializeField] private MapView mapView;
        [SerializeField] private Panel panel;

        public IMapView MapView => mapView;
        public Transform BlockParent => transform;

        public IPanel Panel => panel;
    }
}
