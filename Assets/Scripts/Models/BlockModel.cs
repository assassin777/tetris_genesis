﻿using System;
using UnityEngine;

namespace Models
{
    public class BlockModel : IBlockModel
    {
        public Vector2Int CurrentSellPosition { get; private set; }
        public Vector2 CurrentUnityPosition { get; private set; }
        public Color Color { get; }
        public bool[,] Cells { get; }
        public float CellPerSecSpeed { get; private set; }

        public int HorizontalLength { get; }
        public int VerticalLength { get; }

        private readonly float _cellSize;

        public event EventHandler OnUnityPositionUpdated;
        public event EventHandler OnMoveStarted;
        public event EventHandler OnStepMade;
        
        public BlockModel(bool[,] cells, float cellPerSecSpeed, float cellSize)
        {
            Cells = cells;
            _cellSize = cellSize;
            CellPerSecSpeed = cellPerSecSpeed;
            
            HorizontalLength = Cells.GetLength(0);
            VerticalLength = Cells.GetLength(1);
            
            Color = RandomColor();
        }

        public void Start()
        {
            OnMoveStarted?.Invoke(this, null);
        }

        public Vector2Int DownCellPosition()
        {
            Vector2Int nextDownPosition = CurrentSellPosition;
            nextDownPosition.x -= 1;
            return nextDownPosition;
        }

        public void Rotate()
        {
            for (int i = 0; i < HorizontalLength / 2; i++) { 
                for (int j = i; j < VerticalLength - i - 1; j++) { 
                    
                    bool temp = Cells[i,j]; 
                    Cells[i, j] = Cells[HorizontalLength - 1 - j, i]; 
                    Cells[HorizontalLength - 1 - j, i] = Cells[HorizontalLength - 1 - i, VerticalLength - 1 - j]; 
                    Cells[HorizontalLength - 1 - i, VerticalLength - 1 - j] = Cells[j, VerticalLength - 1 - i]; 
                    Cells[j, HorizontalLength - 1 - i] = temp; 
                } 
            } 
        }

        public void SetPositions(Vector2Int currentBlockSellPosition, Vector2 currentBlockUnityPosition)
        {
            CurrentSellPosition = currentBlockSellPosition;
            CurrentUnityPosition = currentBlockUnityPosition;
            
            OnUnityPositionUpdated?.Invoke(this, new UpdateUnityPositionEventArgs(CurrentUnityPosition));
        }

        public void MakeStep(StepDirection stepDirection)
        {
            Vector2 nextUnityPosition = new Vector2(CurrentUnityPosition.x + _cellSize, CurrentSellPosition.y);
            
            OnStepMade?.Invoke(this, new MadeStepEventArgs(nextUnityPosition, CellPerSecSpeed));
        }

        private Color RandomColor()
        {
            System.Random random = new System.Random();
            var randomVal = random.Next(0, 5);

            switch (randomVal)
            {
                case 1: return Color.red;
                case 2: return Color.green;
                case 3: return Color.blue;
                case 4: return Color.yellow;
                case 5: return Color.cyan;
            }
            
            return Color.white;
        }
    }

    public class UpdateUnityPositionEventArgs : EventArgs
    {
        public readonly Vector2 NewUnityPosition;

        public UpdateUnityPositionEventArgs(Vector2 newUnityPosition)
        {
            NewUnityPosition = newUnityPosition;
        }
    }
    
    public class MadeStepEventArgs : EventArgs
    {
        public readonly Vector2 NextUnityPosition;
        public readonly float TimeInSeconds;

        public MadeStepEventArgs(Vector2 nextUnityPosition, float timeInSeconds)
        {
            NextUnityPosition = nextUnityPosition;
            TimeInSeconds = timeInSeconds;
        }
    }
    
    public enum StepDirection
    {
        Down,
        Left,
        Right
    }
}
