﻿using UnityEngine;

namespace Models.Factories.Map
{
    public class MapModelFactory : IMapModelFactory
    {
        public IMapModel MapModel(float cellSize)
        {
            return CreateModel(Camera.main.orthographicSize, Screen.height, Screen.width, cellSize);
        }

        private IMapModel CreateModel(float halfVerticalCameraSize, int height, int width, float cellSize)
        {
            float halfHorizontalCameraSize = (halfVerticalCameraSize / height) * width;
            
            Vector2Int size = new Vector2Int(
                (int)(halfHorizontalCameraSize * 0.5f / cellSize),
                (int)(halfVerticalCameraSize * 0.5f / cellSize));
            
            Vector2 startPosition = new Vector2(
                (halfHorizontalCameraSize % cellSize) - halfHorizontalCameraSize,
                ((halfVerticalCameraSize % cellSize) * 2) - halfVerticalCameraSize);
            
            IMapModel mapModel = new MapModel(size, startPosition, cellSize);
            return mapModel;
        }
    }
}
