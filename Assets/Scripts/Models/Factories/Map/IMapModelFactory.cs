﻿namespace Models.Factories.Map
{
    public interface IMapModelFactory
    {
        IMapModel MapModel(float cellSize);
    }
}
