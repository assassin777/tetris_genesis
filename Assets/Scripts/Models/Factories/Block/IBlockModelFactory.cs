﻿namespace Models.Factories.Block
{
    public interface IBlockModelFactory
    {
        IBlockModel BlockModel(BlockTypes blockType, float cellPerSecSpeed, float cellSize);
    }
}
