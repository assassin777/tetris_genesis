﻿using BalanceConfigs;
using UnityEngine;

namespace Models.Factories.Block
{
    public class BlockModelFactory : IBlockModelFactory
    {
        public IBlockModel BlockModel(BlockTypes blockType, float cellPerSecSpeed, float cellSize)
        {
            ComplexBlockConfig config = Resources.Load<ComplexBlockConfig>("");
            BlockConfig blockConfig = config.BlockConfigs[blockType];
            bool[,] cells = new bool[blockConfig.FiledCellPositions.Length,blockConfig.FiledCellPositions.Length];

            for (int i = 0; i < blockConfig.FiledCellPositions.Length; i++)
            {
                Vector2Int filledPosition = blockConfig.FiledCellPositions[i];
                cells[filledPosition.x, filledPosition.y] = true;
            }
            IBlockModel blockModel = new BlockModel(cells, cellPerSecSpeed, cellSize);
            return blockModel;
        }
    }
}
