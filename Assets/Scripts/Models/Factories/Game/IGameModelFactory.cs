﻿namespace Models.Factories
{
    public interface IGameModelFactory
    {
        IGameModel GameModel();
    }
}
