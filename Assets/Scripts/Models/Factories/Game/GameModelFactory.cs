﻿using BalanceConfigs;
using UnityEngine;

namespace Models.Factories
{
    public class GameModelFactory : IGameModelFactory
    {
        public IGameModel GameModel()
        {
            GameConfig config = Resources.Load<GameConfig>("");
            IGameModel gameModel = new GameModel(config.CellPerSecSpeed, config.CellSize, config.BlocksNumberByType);
            return gameModel;
        }
    }
}
