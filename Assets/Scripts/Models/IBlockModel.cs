﻿using System;
using UnityEngine;

namespace Models
{
    public interface IBlockModel
    {
        Vector2Int CurrentSellPosition { get; }
        Vector2 CurrentUnityPosition { get; }
        Color Color { get; }
        bool[,] Cells { get; }
        float CellPerSecSpeed { get;}
        
        int HorizontalLength { get; }
        int VerticalLength { get; }
        
        event EventHandler OnUnityPositionUpdated;
        event EventHandler OnMoveStarted;
        event EventHandler OnStepMade;
        
        void Rotate();
        void SetPositions(Vector2Int currentBlockSellPosition, Vector2 currentBlockUnityPosition);
        void Start();
        Vector2Int DownCellPosition();
        void MakeStep(StepDirection stepDirection);
    }
}
