﻿using UnityEngine;

namespace Models
{
    public class MapModel : IMapModel
    {
        public bool GridDirty { get; private set; }
        public Vector2Int Size { get; }
        public Vector2 StartPosition { get; }
        public float SellSize { get; }
        public GridSellModel[,] SellModels { get; }

        public MapModel(Vector2Int size, Vector2 startPosition, float gridSellSize)
        {
            SellModels = new GridSellModel[size.x,size.y];
            SellSize = gridSellSize;
            GridDirty = false;
            Size = size;
            StartPosition = startPosition;

            for (int h = 0; h < Size.x; h++)
            {
                for (int v = 0; v < Size.y; v++)
                {
                    SellModels[h, v] = new GridSellModel();
                }
            }
        }
        
        public bool DoesMapHasFreeCellsForBlock(IBlockModel blockModel, Vector2Int blockSellPosition)
        {
            for (int i = 0; i < blockModel.HorizontalLength / 2; i++) { 
                for (int j = i; j < blockModel.VerticalLength - i - 1; j++)
                {
                    if(!blockModel.Cells[i,j])
                        continue;
                    
                    int mapPositionX = i + blockSellPosition.x;
                    int mapPositionY = j + blockSellPosition.y;
                    
                    if(mapPositionX >= Size.x || mapPositionX < 0)
                        continue;

                    if (mapPositionY >= Size.y || mapPositionY < 0)
                        continue;
                    
                    if (SellModels[mapPositionX, mapPositionY].IsFilled)
                    {
                        return false;
                    }
                } 
            }

            return true;
        }
        
        public bool DoesBlockTraverseMapBorder(IBlockModel blockModel, Vector2Int nextBlockCellPosition)
        {
            for (int i = 0; i < blockModel.HorizontalLength / 2; i++) { 
                for (int j = i; j < blockModel.VerticalLength - i - 1; j++)
                {
                    if(!blockModel.Cells[i,j])
                        continue;
                    
                    int mapPositionX = i + nextBlockCellPosition.x;
                    int mapPositionY = j + nextBlockCellPosition.y;

                    if (mapPositionX >= Size.x || mapPositionX < 0)
                        return true;

                    if (mapPositionY < 0)
                        return true;
                } 
            }

            return false;
        }
    }
}
