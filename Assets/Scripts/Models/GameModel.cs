﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Models
{
    public class GameModel : IGameModel
    {
        public readonly Dictionary<BlockTypes, int> BlocksNumberByType = new Dictionary<BlockTypes, int>();
        public IMapModel MapModel { get; private set; }
        public IBlockModel CurrentBlockModel { get; private set; }
        public float CellPerSecSpeed { get; }
        public float CellSize { get; }


        public event EventHandler OnGameFinished;
        public event EventHandler OnBlockCreated;
        public void FinishGame()
        {
            OnGameFinished?.Invoke(this, null);
        }

        public GameModel(float cellPerSecSpeed, float cellSize, Dictionary<BlockTypes, int> blocksNumberByType)
        {
            CellPerSecSpeed = cellPerSecSpeed;
            CellSize = cellSize;
            
            foreach (var configBlock in blocksNumberByType)
            {
                BlocksNumberByType.Add(configBlock.Key, configBlock.Value);
            }
        }
        
        public BlockTypes NextBlockType()
        {
            Array values = Enum.GetValues(typeof(BlockTypes));
            System.Random random = new System.Random();
            BlockTypes blockType = (BlockTypes)values.GetValue(random.Next(values.Length));

            while (!BlocksNumberByType.ContainsKey(blockType))
            {
                blockType = (BlockTypes)values.GetValue(random.Next(values.Length));
            }

            RemoveBlock(blockType);
            return blockType;
        }

        public void CreateBlock()
        {
            OnBlockCreated?.Invoke(this, null);
        }
        
        public void FindStartPosition()
        {
            Vector2Int currentBlockSellPosition;
            System.Random random = new System.Random();
            do
            {
                currentBlockSellPosition = new Vector2Int(random.Next(0, MapModel.Size.x - 4), MapModel.Size.y - 1);
            } 
            while (!MapModel.DoesMapHasFreeCellsForBlock(CurrentBlockModel, currentBlockSellPosition));
            
            var currentBlockUnityPosition = new Vector2(currentBlockSellPosition.x * MapModel.SellSize,
                currentBlockSellPosition.y * MapModel.SellSize) + MapModel.StartPosition;

            CurrentBlockModel.SetPositions(currentBlockSellPosition, currentBlockUnityPosition);
        }

        public void AddMap(IMapModel mapModel)
        {
            MapModel = mapModel;
        }
        
        public void AddBlock(IBlockModel blockModel)
        {
            CurrentBlockModel = blockModel;
        }

        private void RemoveBlock(BlockTypes blockType)
        {
            int countLeft = BlocksNumberByType[blockType];
            countLeft--;
            if (countLeft == 0)
            {
                BlocksNumberByType.Remove(blockType);
            }
            else
            {
                BlocksNumberByType[blockType] = countLeft;
            }
        }
    }
}
