﻿using UnityEngine;

namespace Models
{
    public interface IMapModel
    {
        bool GridDirty { get; }
        Vector2Int Size { get; }
        Vector2 StartPosition { get; }
        float SellSize { get; }
        GridSellModel[,] SellModels { get; }
        bool DoesMapHasFreeCellsForBlock(IBlockModel blockModel, Vector2Int blockSellPosition);
        bool DoesBlockTraverseMapBorder(IBlockModel blockModel, Vector2Int nextBlockCellPosition);
    }
}
