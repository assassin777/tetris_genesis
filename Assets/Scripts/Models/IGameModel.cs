﻿using System;

namespace Models
{
    public interface IGameModel
    {
        IMapModel MapModel { get; }
        IBlockModel CurrentBlockModel { get; }
        float CellPerSecSpeed { get; }
        float CellSize { get;}

        event EventHandler OnGameFinished;
        event EventHandler OnBlockCreated;
        
        void FinishGame();
        BlockTypes NextBlockType();
        void CreateBlock();
        void FindStartPosition();
        void AddMap(IMapModel mapModel);
        void AddBlock(IBlockModel blockModel);
    }
}
