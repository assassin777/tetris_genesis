﻿using UnityEngine;

namespace Models
{
    public class GridSellModel
    {
        public bool IsFilled { get; set; }
        public Color Color { get; set; }
    }
}
