﻿namespace FSM
{
    public abstract class State : IState
    {
        protected readonly IStateMachine StateMachine;

        protected State(IStateMachine stateMachine)
        {
            StateMachine = stateMachine;
        }

        public abstract void EnterState();
    }
}
