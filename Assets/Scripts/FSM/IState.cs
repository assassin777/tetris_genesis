﻿namespace FSM
{
    public interface IState
    {
        void EnterState();
    }

    public enum StateType
    {
        CheckFilledLine,
        RemoveFilledLine,
        StartGame,
        CheckNextSells,
        FinishGame,
        FinishMoving,
        StartMoving,
        Rotate,
        Move,
    }
}
