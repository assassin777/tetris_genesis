﻿namespace FSM
{
    public interface IStateMachine
    {
        IState State { get; }

        void SetState(StateType stateType);
    }
}
