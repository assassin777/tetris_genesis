﻿using Models;

namespace FSM.States
{
    public class StartGameState : State
    {
        private readonly IMapModel _mapModel;
        private readonly IGameModel _gameModel;

        public StartGameState(IStateMachine stateMachine, IGameModel gameModel, IMapModel mapModel) : base(stateMachine)
        {
            _gameModel = gameModel;
            _mapModel = mapModel;
        }
        
        public override void EnterState()
        {
            if (HasFreeCellsToStartMoving())
            {
                _gameModel.CreateBlock();
                StateMachine.SetState(StateType.StartMoving);
            }
            else
            {
                _gameModel.FinishGame();
            }
        }
        
        private bool HasFreeCellsToStartMoving()
        {
            for (int i = 0; i < _mapModel.Size.x; i++)
            {
                if (!_mapModel.SellModels[i, _mapModel.Size.y -1].IsFilled)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
