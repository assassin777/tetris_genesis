﻿using Models;

namespace FSM.States
{
    public class StartMovingDownState : State
    {
        private readonly IBlockModel _blockModel;
        private readonly IStateMachine _stateMachine;

        public StartMovingDownState(IStateMachine stateMachine, IBlockModel blockModel) : base(stateMachine)
        {
            _stateMachine = stateMachine;
            _blockModel = blockModel;
        }
        
        public override void EnterState()
        {
            _blockModel.Start();
            _stateMachine.SetState(StateType.CheckNextSells);
        }
    }
}
