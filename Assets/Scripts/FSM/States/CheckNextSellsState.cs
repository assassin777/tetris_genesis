﻿using Models;
using UnityEngine;

namespace FSM.States
{
    public class CheckNextSellsState : State
    {
        private readonly IGameModel _gameModel;

        public CheckNextSellsState(IStateMachine stateMachine, IGameModel gameModel) : base(stateMachine)
        {
            _gameModel = gameModel;
        }
        
        public override void EnterState()
        {
            Vector2Int nextDownPosition = _gameModel.CurrentBlockModel.DownCellPosition();

            if (_gameModel.MapModel.DoesMapHasFreeCellsForBlock(_gameModel.CurrentBlockModel, nextDownPosition))
            {
                StateMachine.SetState(StateType.FinishMoving);
            }

            if (_gameModel.MapModel.DoesBlockTraverseMapBorder(_gameModel.CurrentBlockModel, nextDownPosition))
            {
                StateMachine.SetState(StateType.FinishMoving);
            }
            
            StateMachine.SetState(StateType.Move);
        }
    }
}
