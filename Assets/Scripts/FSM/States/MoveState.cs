﻿using Models;

namespace FSM.States
{
    public class MoveState : State
    {
        private readonly IGameModel _gameModel;
        private readonly IStateMachine _stateMachine;

        public MoveState(IStateMachine stateMachine, IGameModel gameModel) : base(stateMachine)
        {
            _gameModel = gameModel;
        }
        
        public override void EnterState()
        {
            _gameModel.CurrentBlockModel.MakeStep(StepDirection.Down);
        }
    }
}
