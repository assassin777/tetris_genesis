﻿using FSM.States;
using Models;

namespace FSM.Factories
{
    public class StateFactory : IStateFactory
    {
        private readonly CheckFilledLineState _checkFilledLineState;
        private readonly RemoveFilledLineState _removeFilledLineState;
        private readonly StartGameState _startGameState;
        private readonly CheckNextSellsState _checkNextSellsState;
        private readonly FinishGameState _finishGameState;
        private readonly FinishMovingState _finishMovingState;
        private readonly StartMovingDownState _startMovingDownState;
        private readonly RotateState _rotateState;
        private readonly MoveState _moveState;
        
        public StateFactory(IStateMachine stateMachine, IGameModel gameModel)
        {
            //_checkFilledLineState = new CheckFilledLineState();
        }
        
        public IState GetState(StateType stateType)
        {
            switch (stateType)
            {
                case StateType.Move: return _moveState;
                case StateType.Rotate: return _rotateState;
                case StateType.FinishGame: return _finishGameState;
                case StateType.FinishMoving: return _finishMovingState;
                case StateType.StartGame: return _startGameState;
                case StateType.StartMoving: return _startMovingDownState;
                case StateType.CheckFilledLine: return _checkFilledLineState;
                case StateType.RemoveFilledLine: return _removeFilledLineState;
                case StateType.CheckNextSells: return _checkNextSellsState;
            }

            return null;
        }
    }
}
