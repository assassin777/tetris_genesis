﻿namespace FSM.Factories
{
    public interface IStateFactory
    {
        IState GetState(StateType stateType);
    }
}
