﻿public enum BlockTypes
{
    Iblock,
    Jblock,
    Lblock,
    Oblock,
    Sblock,
    Tblock,
    Zblock
}
